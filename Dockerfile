FROM node:16-alpine

RUN apk --no-cache add bash curl jq git && npm install --global gh-pages@3.0.0

COPY pipe.sh /

RUN chmod a+x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
