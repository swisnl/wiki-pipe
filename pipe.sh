#!/usr/bin/env bash

set -e

# required parameters
CLIENT_KEY=${CLIENT_KEY:?'CLIENT_KEY environment variable missing.'}
CLIENT_SECRET=${CLIENT_SECRET:?'CLIENT_SECRET environment variable missing.'}

# default parameters
DIR=${DIR:="docs"}
GIT_USER_EMAIL=${GIT_USER_EMAIL:="commits-noreply@bitbucket.org"}
GIT_USER_NAME=${GIT_USER_NAME:="bitbucket-pipelines"}

# fetch access token
ACCESS_TOKEN=$(curl -s -X POST -u "${CLIENT_KEY}:${CLIENT_SECRET}" \
  https://bitbucket.org/site/oauth2/access_token \
  -d grant_type=client_credentials -d scopes="repository" | jq --raw-output '.access_token')

# set user info
git config --global user.email "${GIT_USER_EMAIL}"
git config --global user.name "${GIT_USER_NAME}"

# push dir to wiki
gh-pages --dist="${DIR}" --branch=master --repo="https://x-token-auth:${ACCESS_TOKEN}@bitbucket.org/${BITBUCKET_REPO_FULL_NAME}.git/wiki"
