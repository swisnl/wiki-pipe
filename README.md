# Bitbucket Pipelines Pipe: Repo to wiki sync

This pipe will sync a directory from your repository to the wiki.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: swisnl/wiki-pipe:1
  variables:
    CLIENT_KEY: '<string>'
    CLIENT_SECRET: '<string>'
    # DIR: '<string>' # Optional.
```

## Variables

| Variable          | Usage                                                                                                    |
| ----------------- |----------------------------------------------------------------------------------------------------------|
| CLIENT_KEY (*)    | OAuth client id used to access your repository wiki. See [prerequisites](#markdown-header-prerequisites) |
| CLIENT_SECRET (*) | OAuth client secret.                                                                                     |
| DIR               | The directory containing the files you want to sync to your wiki. Default: `docs`.                       |
| GIT_USER_EMAIL    | The commit author email. Default: `commits-noreply@bitbucket.org`.                                       |
| GIT_USER_NAME     | The commit author name. Default: `bitbucket-pipelines`.                                                  |

_(*) = required variable._

## Prerequisites

To use this pipe, you need a (private) OAuth consumer key and secret with wiki read and write permission. You can follow the instructions [here](https://support.atlassian.com/bitbucket-cloud/docs/push-back-to-your-repository/#OAuth) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: swisnl/wiki-pipe:1
    variables:
      CLIENT_KEY: $WIKI_PIPE_CLIENT_KEY
      CLIENT_SECRET: $WIKI_PIPE_CLIENT_SECRET
```

Advanced example:

```yaml
script:
  - pipe: swisnl/wiki-pipe:1
    variables:
      CLIENT_KEY: $WIKI_PIPE_CLIENT_KEY
      CLIENT_SECRET: $WIKI_PIPE_CLIENT_SECRET
      DIR: "wiki"
      GIT_USER_EMAIL: "wiki-bot@swis.nl"
      GIT_USER_NAME: "Wiki Bot"
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know. The pipe is maintained by info@swis.nl.

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce
